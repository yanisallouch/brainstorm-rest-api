package fr.umfds.TPtestServicesREST;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.glassfish.jersey.internal.inject.AbstractBinder;
import org.glassfish.jersey.logging.LoggingFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

public class BrainstormRessourceTestIT extends JerseyTest {
	
	protected Application configure() {
		ResourceConfig resourceConfig = new ResourceConfig(BrainstormRessource.class);
		resourceConfig.property(LoggingFeature.LOGGING_FEATURE_LOGGER_LEVEL_SERVER, Level.WARNING.getName());

		/*
		 * Ce register n'est pas correct.
		 * Le problème du mock suivant, c'est que ca fonctionne meme sans le @Inject sur ressource BrainstormDB de la classe BrainstormRessource
		 * (qui mock toujours le resultat retourné par la classe qui fait l'accès a la BDD).
		 */
		/*
		 * On dois donc mocké une classe de type BrainstormDB qui est injecté ou non en fonction de l'annotatation dans la classe BrainstormRessource.
		 * 
		 * Si l'annotation @Inject est présente, JerseyTest pourra tout a "bind" notre mock a celui utilisé par la classe BrainstormRessource,
		 * on aura alors 3 items retourné par notre API.
		 * 
		 * Sinon l'annotation @Inject n'est pas présente, JerseyTest ne saura pas quoi faire du "bind"  et nos résultat proviennent directement de la BDD,
		 * on aura alors 5 items retourné par notre API.
		 */
		resourceConfig.register(new AbstractBinder() {
			private Brainstorm b1 = new Brainstorm("InjectéA", 1, new ArrayList<Idea>());
			private Brainstorm b2 = new Brainstorm("InjectéB", 2, new ArrayList<Idea>());
			private Brainstorm b3 = new Brainstorm("InjectéC", 3, new ArrayList<Idea>());

			@Override
			protected void configure() {
				List<Brainstorm> l = Arrays.asList(this.b1, this.b2, this.b3);
				// j'ai juste du ajouté la déclaration du mock et son initialisation avant, vu qu'elle n'est pas donnée dans le sujet de TP.
				BrainstormDB dbMock = Mockito.mock(BrainstormDB.class);
				Mockito.when(dbMock.getBrainstormsv2()).thenReturn(l);
				bind(dbMock).to(BrainstormDB.class);
			}
		});


		return resourceConfig;
	}

	/*
	 * Il faut changer la valeur 'expected' de 5 a 3 et vice versa en fonction de si nous voulons ou non controlé le contenu (injecté).
	 */
	@Test
	public void testGetBrainstorms() {
		// Given
		int expectedSize = 3;
		
		// When
		Response response = this.target("/brainstorms").request(MediaType.APPLICATION_JSON_TYPE).get();
		
		// Then
		Assert.assertEquals("Http Response should be 200: ", Status.OK.getStatusCode(), response.getStatus());
		List<Brainstorm> readEntities = response.readEntity(new GenericType<List<Brainstorm>>() {});
		Assert.assertNotNull(readEntities);
		int actualSize = readEntities.size();
		Assert.assertEquals(expectedSize, actualSize);
		Assert.assertTrue(readEntities.stream().anyMatch(current -> current.getIdentifiant() == 1));
	}
	
	@Test
	public void testGetUnknownBrainstormID() {
		// Given
		int unknownID = 69;
		// When
		Response response = this.target("/brainstorms/brainstormid-"+ unknownID).request(MediaType.APPLICATION_JSON_TYPE).get();
		// Then
		Assert.assertEquals("Http Response should be 404: ", Status.NOT_FOUND.getStatusCode(), response.getStatus());
		Brainstorm readEntities = response.readEntity(new GenericType<Brainstorm>() {
		});
		Assert.assertNull(readEntities);
	}
	
	@Test
	public void testPostNewBrainstorm() {
		// Given
		Brainstorm expected = new Brainstorm("postIT", 99, new ArrayList<Idea>());
		Client client = ClientBuilder.newClient();
		WebTarget target = client.target(("http://localhost:9998/brainstorms"));
		
		// When
		Response response = target.request(MediaType.APPLICATION_JSON)
		                        .accept(MediaType.APPLICATION_JSON_TYPE)
		                        .post(Entity.json(expected), Response.class);
		Brainstorm readEntities = response.readEntity(new GenericType<Brainstorm>(){});
		
		// Then
		assertAll(
				() -> Assert.assertEquals("Http Response should be 201 : ", Status.CREATED.getStatusCode(), response.getStatus()),
				() -> Assert.assertNotNull(readEntities),
				() -> assertEquals(expected, readEntities)
		);
	}
	
	@Test
	public void testPutNewIdeaInChoosenBrainstorm() {
		// Given
		int ID = 0;
		Idea expectedIdea = new Idea("Je suis dans le brainstorm d'id " + ID);
		String expected = "{\"idea\":\"" + expectedIdea + "\"}";
		Client client = ClientBuilder.newClient();
		WebTarget target = client.target("http://localhost:9998/brainstorms/brainstorm/"+ID+"/ideas");
		
		// When
		Response response = target.request(MediaType.APPLICATION_JSON).put(Entity.entity(expectedIdea, MediaType.APPLICATION_JSON));
		String readEntities = response.readEntity(new GenericType<String>(){});
		
		// Then
		assertAll(
				() -> Assert.assertEquals("Http Response should be 201 : ", Status.CREATED.getStatusCode(), response.getStatus()),
				() -> Assert.assertNotNull(readEntities),
				() -> assertEquals(expected, readEntities)
		);
	}
	
	@Test
	public void testGetIdeaFromChosenBrainstorm() {
		// Given
		int ID = 1;
		Idea expectedIdea = new Idea("Je suis dans le brainstorm d'id " + ID);
		Client client = ClientBuilder.newClient();
		WebTarget target = client.target("http://localhost:9998/brainstorms/brainstorm/"+ID+"/ideas");
		
		// When
		Response tmp = target.request(MediaType.APPLICATION_JSON).put(Entity.entity(expectedIdea, MediaType.APPLICATION_JSON));
		tmp = target.request(MediaType.APPLICATION_JSON).put(Entity.entity(expectedIdea, MediaType.APPLICATION_JSON));
		tmp = target.request(MediaType.APPLICATION_JSON).put(Entity.entity(expectedIdea, MediaType.APPLICATION_JSON));
		Response response = this.target("/brainstorms/brainstorm/"+ID+"/ideas").request(MediaType.APPLICATION_JSON_TYPE).get();
		List<Idea> readEntities = response.readEntity(new GenericType<List<Idea>>() {});
		
		// Then
		assertAll(
				() -> Assert.assertEquals("Http Response should be 200: ", Status.OK.getStatusCode(), response.getStatus()),
				() -> Assert.assertNotNull(readEntities)
		);
	}
}