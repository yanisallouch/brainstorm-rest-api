package fr.umfds.TPtestServicesREST;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;

class BrainstormRessourceTest {

	private Brainstorm b1;
	private Brainstorm b2;
	private Brainstorm b3;
	private Brainstorm b4;
	private Brainstorm b5;

	private BrainstormDB bd;
	private BrainstormRessource br;

	@Mock
	private BrainstormDB bdMock;

	@BeforeEach
	public void setUp() {
		this.b1 = new Brainstorm("nom1", 1, new ArrayList<Idea>());
		this.b2 = new Brainstorm("nom2", 2, new ArrayList<Idea>());
		this.b3 = new Brainstorm("nom3", 3, new ArrayList<Idea>());
		this.b4 = new Brainstorm("nom4", 4, new ArrayList<Idea>());
		this.b5 = new Brainstorm("nom5", 5, new ArrayList<Idea>());

		this.bd = new BrainstormDB();
		this.br = new BrainstormRessource(this.bd);
		
		/* 
		 * Remarque : A la premiere exception de type:
		 * java.lang.IllegalStateException: Could not initialize plugin : interface org.mockito.plugins.MockMaker
		 * Verifier que le JDK (1.8 en fonction du parametre de Maven) est utiliser !
		 */
		
		this.bdMock = Mockito.mock(BrainstormDB.class);
	}

	@Test
	public void getBrainstormNotEmpty() {
		// Given
		int expected = 5;
		
		// When
		
		// Then
		assertAll(
			() -> assertEquals(expected, this.br.getBrainstorms().size()),
			() -> assertTrue(this.br.getBrainstorms().size() > 0)
		);
	}
	
	@Test
	public void getBrainstormIsSorted() {
		// Given
		List<Brainstorm> expectedSorted = new ArrayList<>(Arrays.asList(this.b1, this.b2, this.b3, this.b4, this.b5));
		List<Brainstorm> unsorted = new ArrayList<>(Arrays.asList(this.b5, this.b4, this.b3, this.b1, this.b2));
		MockedStatic<BrainstormDB> db = Mockito.mockStatic(BrainstormDB.class);
		
		// When
		db.when(BrainstormDB::getBrainstorms).thenReturn(unsorted);
		List<Brainstorm> actual = this.br.getBrainstorms();
		
		// Then
		assertAll(
				() -> assertEquals(expectedSorted.size(), actual.size()),
				() -> assertArrayEquals(expectedSorted.toArray(), actual.toArray())
		);
	}

	@Test
	public void getBrainstormIsSortedv2() {
		// Given
		List<Brainstorm> expected = new ArrayList<>(Arrays.asList(this.b1, this.b2, this.b3, this.b4, this.b5));
		List<Brainstorm> unsorted = new ArrayList<>(Arrays.asList(this.b5, this.b4, this.b2, this.b3, this.b1));
		this.br.setDb(this.bdMock);
		
		// When
		Mockito.when(this.bdMock.getBrainstormsv2()).thenReturn(unsorted);
		
		// Then
		List<Brainstorm> actual = this.br.getBrainstorms();
		assertAll(
				() -> assertEquals(expected.size(), actual.size()),
				() -> assertArrayEquals(expected.toArray(), actual.toArray())
		);
	}
}