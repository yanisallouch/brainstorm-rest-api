package fr.umfds.TPtestServicesREST;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.ws.rs.NotFoundException;

public class BrainstormDB {

	private static List<Brainstorm> encours = Arrays.asList(
			new Brainstorm("nonInjecteNom1", 1, new ArrayList<Idea>()), new Brainstorm("nonInjecteNom2", 2, new ArrayList<Idea>()),
			new Brainstorm("nonInjecteNom4", 4, new ArrayList<Idea>()), new Brainstorm("nonInjecteNom3", 3, new ArrayList<Idea>()),
			new Brainstorm("nonInjecteNom5", 5, new ArrayList<Idea>()));

	public static List<Brainstorm> getBrainstorms() {
		return encours;
	}

	public void addBrainstorm(Brainstorm brainstorm) {
		encours.add(brainstorm);
	}
	
	/*
	 * Cette methode est mocker pour le test d'integration.
	 */
	public List<Brainstorm> getBrainstormsv2() {
		return encours;
	}

	public void addIdea(int brainstormId, String idea) {
		Brainstorm bID = encours.get(brainstormId);
		if(bID != null) {
			bID.addIdea(idea);
			encours.remove(brainstormId);
			encours.add(brainstormId, bID);
		}else {
			throw new NotFoundException();
		}

	}
}
