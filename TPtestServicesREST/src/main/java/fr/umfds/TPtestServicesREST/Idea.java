package fr.umfds.TPtestServicesREST;

public class Idea {
	private String idea;
	
	public Idea(String idea) {
		this.idea = idea;
	}
	
	public String getIdea() {
		return this.idea;
	}
	
	public void setIdea(String idea) {
		this.idea = idea;
	}

	@Override
	public String toString() {
		return idea;
	}
	
	
}
