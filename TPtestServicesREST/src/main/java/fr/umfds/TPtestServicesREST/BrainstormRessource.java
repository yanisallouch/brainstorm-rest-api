package fr.umfds.TPtestServicesREST;

import java.util.Collections;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

@Path("/brainstorms")
@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
public class BrainstormRessource {

	/*
	 * Pour le test d'integration, un-comment l'annotation @Inject suivante.
	 * Par la suite, Ctrl + Shift + O : pour l'importer.
	 */
	@Inject
	public BrainstormDB db = new BrainstormDB();

	public BrainstormRessource() {
	}

	public BrainstormRessource(BrainstormDB db) {
		this.db = db;
	}

	/*
	 * Pour le test d'integration, il faut utiliser la getBrainstormsv2() non statique qui permet d'être mocké.
	 * Dans tous les cas, la méthode v2 fonctionne.
	 * Pour rester compliant avec le sujet de TP, j'ai laisser en commentaire l'appel a la méthode statique getBrainstorms(). 
	 */
	@GET
	public List<Brainstorm> getBrainstormsv2() {
		List<Brainstorm> sorted;
		sorted = this.db.getBrainstormsv2();

		Collections.sort(sorted);
		return sorted;
	}

	public List<Brainstorm> getBrainstorms() {
		List<Brainstorm> sorted;
		sorted = BrainstormDB.getBrainstorms();

		Collections.sort(sorted);
		return sorted;
	}

	@GET
	@Path("brainstormid-{id}")
	public Brainstorm getBrainstorm(@PathParam("id")int brainstormId) {
		List<Brainstorm> res = this.db.getBrainstormsv2();
		boolean isEquals = false;
		for (Brainstorm brainstorm : res) {
			isEquals = brainstormId == brainstorm.getIdentifiant();
			if (isEquals) {
				return brainstorm; 
			}
		}
		throw new NotFoundException();
	}
	
	@POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addBrainstorm(Brainstorm brainstorm) {
		db.addBrainstorm(brainstorm);
        return Response.ok(brainstorm).status(Status.CREATED).build();
    }
	
	@PUT
    @Consumes(MediaType.APPLICATION_JSON)
	@Path("/brainstorm/{id}/ideas")
    public Response addIdeaToBrainstorm(@PathParam("id")int brainstormId, String idea) {
		db.addIdea(brainstormId,idea);
        return Response.ok(idea).status(Status.CREATED).build();
    }
	
	@GET
	@Path("/brainstorm/{id}/ideas")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Idea> getBrainstormIdeas(@PathParam("id")int brainstormId) {
		List<Brainstorm> res = this.db.getBrainstormsv2();
		boolean isEquals = false;
		for (Brainstorm brainstorm : res) {
			isEquals = brainstormId == brainstorm.getIdentifiant();
			if (isEquals) {
				return brainstorm.getIdeas(); 
			}
		}
		throw new NotFoundException();
	}
	
	public BrainstormDB getDb() {
		return this.db;
	}

	public void setDb(BrainstormDB db) {
		this.db = db;
	}

}
