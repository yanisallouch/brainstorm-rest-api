package fr.umfds.TPtestServicesREST;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "brainstorm")
@XmlAccessorType(XmlAccessType.FIELD)
public class Brainstorm implements Comparable<Brainstorm> {
	private String nom;
	private int identifiant;
	private List<Idea> ideas = new ArrayList<Idea>();

	public Brainstorm() {
		super();
	}

	public Brainstorm(String nom, int identifiant, List<Idea> ideas) {
		super();
		this.nom = nom;
		this.identifiant = identifiant;
		this.ideas = ideas;
	}

	public String getNom() {
		return this.nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getIdentifiant() {
		return this.identifiant;
	}

	public void setIdentifiant(int identifiant) {
		this.identifiant = identifiant;
	}

	public List<Idea> getIdeas() {
		return this.ideas;
	}

	public void setIdeas(List<Idea> ideas) {
		this.ideas = ideas;
	}
	
	public void addIdea(String idea) {
		ideas.add(new Idea(idea));
	}

	@Override
	public int compareTo(Brainstorm o) {
		return this.getNom().compareTo(o.getNom());
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.identifiant, this.nom);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		Brainstorm other = (Brainstorm) obj;
		if (this.identifiant != other.identifiant) {
			return false;
		}
		if (this.nom == null) {
			if (other.nom != null) {
				return false;
			}
		} else if (!this.nom.equals(other.nom)) {
			return false;
		}
		return true;
	}
}